package com.example.crudappitsm

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.crudappitsm.databinding.ActivityReadDataBinding
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.DatabaseReference


class ReadData : AppCompatActivity() {

    private lateinit var binding : ActivityReadDataBinding
    private lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReadDataBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnReadData.setOnClickListener {
            val nombre : String = binding.edtNombreD.text.toString()

            if(nombre.isNotEmpty()){
                readData(nombre)
            }else{
                Toast.makeText(this,"Ingrese un usuario correcto", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun readData(nombre: String) {

        database = FirebaseDatabase.getInstance().getReference("Usuarios")
        database.child(nombre).get().addOnSuccessListener {
            if (it.exists()){
                val nombre = it.child("nombre").value
                val apellidoP = it.child("apellidoP").value
                val edad = it.child("edad").value
                val ncontrol = it.child("ncontrol").value
                val carrera = it.child("carrera").value
                val escolaridad = it.child("escolaridad").value

                binding.edtNombreD.text.clear()
                binding.tvNombre.text = nombre.toString()
                binding.tvApellidoP.text = apellidoP.toString()
                binding.tvEdad.text = edad.toString()
                binding.tvNcontrol.text = ncontrol.toString()
                binding.tvCarrera.text = carrera.toString()
                binding.tvEscolaridad.text = escolaridad.toString()
            }else{
                Toast.makeText(this,"El usuario no existe", Toast.LENGTH_SHORT).show()
            }
        }.addOnFailureListener {
            Toast.makeText(this,"Error!!!", Toast.LENGTH_SHORT).show()
        }

    }
}