package com.example.crudappitsm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.crudappitsm.databinding.ActivityMainBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //database.setValue("Hola Misantla")
        binding.btnIngresar.setOnClickListener {
            val nombre = binding.edtNombre.text.toString()
            val apellido = binding.edtApellido.text.toString()
            val edad = binding.edtEdad.text.toString()
            val ncontrol = binding.edtNcontrol.text.toString()
            val carrera = binding.edtCarrera.text.toString()
            val escolaridad = binding.edtEscolaridad.text.toString()

            database = FirebaseDatabase.getInstance().getReference("Usuarios")
            val usuario = User(nombre, apellido, edad, ncontrol, carrera, escolaridad)
            database.child(nombre).setValue(usuario).addOnSuccessListener {

                binding.edtNombre.text.clear()
                binding.edtApellido.text.clear()
                binding.edtEdad.text.clear()
                binding.edtNcontrol.text.clear()
                binding.edtCarrera.text.clear()
                binding.edtEscolaridad.text.clear()

                Toast.makeText(this, "Se guardó correctamente", Toast.LENGTH_SHORT).show()
            }.addOnFailureListener {
                Toast.makeText(this, "No se guardó la información", Toast.LENGTH_SHORT).show()
            }



            binding.btnLeerDatos.setOnClickListener {
                val intent = Intent(this, ReadData::class.java)
                startActivity(intent)
            }

            binding.btnEliminar.setOnClickListener {
                val intent = Intent(this, DeleteData::class.java)
                startActivity(intent)
            }

            binding.btnActualizar.setOnClickListener {
                val intent = Intent(this, Search::class.java)
                startActivity(intent)

            }
        }
    }
}


