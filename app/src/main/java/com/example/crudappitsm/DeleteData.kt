package com.example.crudappitsm

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.crudappitsm.databinding.ActivityDeleteDataBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class DeleteData : AppCompatActivity() {

    private lateinit var binding : ActivityDeleteDataBinding
    private  var reference : DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDeleteDataBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.btnEliminar.setOnClickListener {
            val username : String = binding.EdtEliminar.getText().toString()

            if(!username.isEmpty()){
                deleteData(username)
            }else{
                Toast.makeText(this, "Ingrese un username", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun deleteData(username: String) {
        reference = FirebaseDatabase.getInstance().getReference("Usuarios")
        reference!!.child(username).removeValue().addOnCompleteListener {
            if(it.isSuccessful){
                Toast.makeText(this,"Eliminado correctamente", Toast.LENGTH_SHORT).show()
                binding.EdtEliminar.text.clear()
            }else{
                Toast.makeText(this,"Ups! Ocurrió un error", Toast.LENGTH_SHORT).show()
            }
        }

    }
}