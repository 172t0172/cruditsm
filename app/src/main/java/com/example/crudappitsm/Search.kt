package com.example.crudappitsm

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.crudappitsm.databinding.ActivitySearchBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class Search : AppCompatActivity() {

    private lateinit var binding : ActivitySearchBinding
    private lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSearchData.setOnClickListener {
            val nombre : String = binding.edtNombreD.text.toString()

            if(nombre.isNotEmpty()){
                readData(nombre)
            }else{
                Toast.makeText(this,"Ingrese un usuario correcto", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun readData(nombre: String) {

        database = FirebaseDatabase.getInstance().getReference("Usuarios")
        database.child(nombre).get().addOnSuccessListener {
            if (it.exists()){
                val nombre = it.child("nombre").value
                val apellidoP = it.child("apellidoP").value
                val edad = it.child("edad").value
                val ncontrol = it.child("ncontrol").value
                val carrera = it.child("carrera").value
                val escolaridad = it.child("escolaridad").value

                binding.edtNombreD.text.clear()
                //binding.tvNombre.text = nombre.toString()
                //binding.tvApellidoP.text = apellidoP.toString()
                //binding.tvEdad.text = edad.toString()
                var bundle = Bundle()
                bundle.apply {
                    putString("key_name",nombre.toString())
                    putString("key_apellidoP",apellidoP.toString())
                    putString("key_edad",edad.toString())
                    putString("key_ncontrol",ncontrol.toString())
                    putString("key_carrera",carrera.toString())
                    putString("key_escolaridad",escolaridad.toString())
                }

                val intent = Intent(this,UpdateData::class.java).apply{
                    putExtras(bundle)
                }
                startActivity(intent)

            }else{
                Toast.makeText(this,"El usuario no existe", Toast.LENGTH_SHORT).show()
            }
        }.addOnFailureListener {
            Toast.makeText(this,"Ocurrió un error!!!", Toast.LENGTH_SHORT).show()
        }

    }
}
