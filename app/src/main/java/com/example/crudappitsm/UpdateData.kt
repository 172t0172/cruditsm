package com.example.crudappitsm

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.crudappitsm.databinding.ActivityUpdateDataBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class UpdateData : AppCompatActivity() {

    private lateinit var binding: ActivityUpdateDataBinding
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateDataBinding.inflate(layoutInflater)
        setContentView(binding.root)
        llenar()

        binding.btnActualizar.setOnClickListener {

            val nombre = binding.EdtNombre.text.toString()
            val apellido = binding.EdtApellidoP.text.toString()
            val edad = binding.EdtEdad.text.toString()
            val ncontrol = binding.EdtNcontrol.text.toString()
            val carrera = binding.EdtCarrera.text.toString()
            val escolaridad = binding.EdtEscolaridad.text.toString()

            updateData(nombre,apellido,edad,ncontrol,carrera,escolaridad)
        }
    }

    private fun llenar() {
        val bundle: Bundle? = intent.extras
        bundle?.let {
            binding.EdtNombre.text.append(it.getString("key_name"))
            binding.EdtApellidoP.text.append(it.getString("key_apellidoP"))
            binding.EdtEdad.text.append(it.getString("key_edad"))
            binding.EdtNcontrol.text.append(it.getString("key_ncontrol"))
            binding.EdtCarrera.text.append(it.getString("key_carrera"))
            binding.EdtEscolaridad.text.append(it.getString("key_escolaridad"))
        }
    }

    private fun updateData(nombre: String, apellido: String, edad: String, ncontrol: String, carrera: String, escolaridad: String) {

        database = FirebaseDatabase.getInstance().getReference("Usuarios")

        val user = mapOf<String,String>(
            "nombre" to nombre,
            "apellidoP" to apellido,
            "edad" to edad,
            "ncontrol" to ncontrol,
            "carrera" to carrera,
            "escolaridad" to escolaridad
        )

        database.child(nombre).updateChildren(user).addOnCompleteListener {
            binding.EdtNombre.text.clear()
            binding.EdtApellidoP.text.clear()
            binding.EdtEdad.text.clear()
            binding.EdtNcontrol.text.clear()
            binding.EdtCarrera.text.clear()
            binding.EdtEscolaridad.text.clear()

            Toast.makeText(this, "Se actualizó correctamente", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(this, "Error! No se logró actualizar ", Toast.LENGTH_SHORT).show()
        }
    }
}